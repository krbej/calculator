<!DOCTYPE html>
<html>

<head>
    <!-- CSS only -->
    <link rel="stylesheet" href="/assets/zapp/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/assets/zapp/font-OpenSans.css" />

    <!-- JS, Popper.js, and jQuery -->
    <script src="/assets/zapp/jquery-3.5.1.slim.min.js"></script>
    <script src="/assets/zapp/popper.min.js"></script>
    <script src="/assets/zapp/bootstrap.min.js"></script>
    <script src="/assets/zapp/bootstrap-slider.min.js"></script>
    <link rel="stylesheet" href="/assets/zapp/bootstrap-slider.min.css" />
    <link rel="stylesheet" href="/assets/zapp/css.css" />
</head>

<body>
    <div class="container-sm">
        <div class="row justify-content-center">
            <div class="col-10">
                <div style="display: block; height: 200px; padding-top: 20px; font-size: 14px;">
                    <h2>Beregn din Zapp-pris her</h2>
                    <p>Vi har gjort det let og overskueligt at regne dit forbrug ud.</p>
                    <p>Med få valg af bil, kørsel og pris per kWh kan vi hjælpe dig med indsigt i dine udgifter mht. strømforbruget til bilen.</p>
                </div>
                <form name="addem" action="" id="addem">
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group mb-3">
                                <select class="custom-select" id="carkWhkm" name="carkWhkm" onchange="calculateTotal()">
                                    <option value="186">MG ZS EV</option>
                                    <option value="165">Renault ZOE</option>
                                    <option value="165">Peugot e208</option>
                                    <option value="140">Honda e</option>
                                    <option value="117">Volkswagen eUP!/SEAT Mii/Skode CITYGO</option>
                                    <option value="153">Kia e-Niro</option>
                                    <option value="155">Volkswagen ID.3</option>
                                    <option value="168">Volkswagen eGolf</option>
                                    <option value="127">Opel Corsa-e</option>
                                    <option value="170">DS 3 Crossback E-Tense</option>
                                    <option value="194">Ford Mustang March-E</option>
                                    <option value="153">Hyundai IONIQ Electric</option>
                                    <option value="153">Tesla Model 3 Standard Range Plus</option>
                                    <option value="156">Mini Cooper SE</option>
                                    <option value="160">Hyundai Kona Electric 64kWh</option>
                                    <option value="161">Tesla Model 3 Long Range Dual Motor</option>
                                    <option value="161">BMW i3 120Ah</option>
                                    <option value="164">Nissan Leaf</option>
                                    <option value="167">Tesla Model 3 Long Range Performance</option>
                                    <option value="184">Tesla Model S Long Range</option>
                                    <option value="188">Tesla Model S Performance</option>
                                    <option value="195">Porsche Taycan 4S</option>
                                    <option value="211">Tesla Model X Long Range</option>
                                    <option value="216">Mercedes-Benz EQC</option>
                                    <option value="216">Tesla Model X Performance</option>
                                    <option selected value="231">Audi e-tron 50 quattro</option>
                                    <option value="232">Jaguar i-Pace</option>
                                    <option value="250">Generel Hybrid SUV</option>
                                    <option value="200">Generel Hybrid Sendan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <p>Antal årlige kilometer<span class="float-right" id="yearlykm-display"></span></p>
                        </div>
                        <div class="col-6">
                            <input style="display: none;" id="yearlykm" type="range" name="yearlykm" oninput="calculateTotal()" onchange="calculateTotal()" data-slider-min="5000" data-slider-value="15000" data-slider-max="50000" data-slider-step="1000" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <p>Gennemsnits kost pr. kWh<span class="float-right" id="costkWh-display"></span></p>
                        </div>
                        <div class="col-6">
                            <input style="display: none;" id="costkWh" type="range" name="costkWh" oninput="calculateTotal()" onchange="calculateTotal()" data-slider-min="0" data-slider-value="2" data-slider-max="5" data-slider-step="0.1" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <p>Førstegangsudgift til elbiloplader inkl. installation</p>
                            <p style="font-size: 12px; padding-top: 4px;">Indtast prisen på opladeren du køber inkl. installation (Indtast 0 kr hvis du allerede har en oplader)</p>
                        </div>
                        <div class="col-6 text-right">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" aria-describedby="basic-addon2" name="oneTimeCost" onkeyup="calculateTotal()" value="0" />
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">DKK</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 font-weight-bold">
                            <p class="mb-1">Abonnement</p>
                            <label class="control control-radio">
                                Zapp Standard
                                <input onchange="calculateTotal()" type="radio" id="Zapp" name="sub" value="Zapp" checked="checked" />
                                <div class="control_indicator"></div>
                            </label>
                            <label class="control control-radio">
                                Zapp m. Monta
                                <input onchange="calculateTotal()" type="radio" id="Monta" name="sub" value="Monta" />
                                <div class="control_indicator"></div>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <p>Abonnement pr. måned:</p>
                        </div>
                        <div class="col-6 text-right">
                            <p><span id="subCost"></span> DKK</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <p>Strøm omkostninger:</p>
                        </div>
                        <div class="col-6 text-right">
                            <p><span id="powerCost"></span> DKK</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <p>Refusion (<span id="refundZapp1"></span> DKK pr. kWh):</p>
                        </div>
                        <div class="col-6 text-right">
                            <p><span id="refund"></span> DKK</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 font-weight-bold">
                            <p class="mb-1">Periode</p>
                            <label class="control control-radio">
                                1 år
                                <input onchange="calculateTotal()" type="radio" id="1year" name="year" value="1" checked="checked" />
                                <div class="control_indicator"></div>
                            </label>
                            <label class="control control-radio">
                                2 år
                                <input onchange="calculateTotal()" type="radio" id="2year" name="year" value="2" />
                                <div class="control_indicator"></div>
                            </label>
                            <label class="control control-radio">
                                3 år
                                <input onchange="calculateTotal()" type="radio" id="3year" name="year" value="3" />
                                <div class="control_indicator"></div>
                            </label>
                        </div>
                    </div>
                </form>
                <div class="row mt-5">
                    <div class="col-6 text-left">
                        <p style="font-size: 24px;">Din månedlige Zapp-pris</p>
                        <p style="font-size: 12px;">Denne omfatter både strøm og abonnement samt evt. installation</p>
                    </div>
                    <div class="col-6 text-right">
                        <span id="mothlyCost"></span> DKK /md
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-12 text-center">
                        <p style="font-size: 28px;">JA, du kan spare med Zapp!</p>
                        <p style="font-size: 12px;" class="mb-0">Med Zapp ejer du din ladestander</p>
                        <p style="font-size: 12px;">Du slipper for en dyr regning når du opsiger din lejeaftale med de andre.</p>
                        <p style="font-size: 12px;">Beregningen er lavet med 100% hjemmeopladning med Zapp Abonnement. Med <span id="refundZapp2"></span> kr/kWh elrefusion.</p>
                    </div>
                </div>
                <div class="row row-cols-1 row-cols-md-3 mt-3 text-center">
                    <div class="col-md4 col-12 mb-4">
                        <div class="card h-100">
                            <div class="card-body">
                                <h5 class="card-title" id="FirstCardTitle"></h5>
                                <p class="card-text" id="FirstCardText"></p>
                                <p class="card-text" style="font-size: 10px;"><span id="FirstCardStartFee"></span><span id="FirstCardSub"></span><span id="FirstCardSub"></span><span id="FirstCardYearlyFee"></span><span id="FirstCardTerminationFee"></span></p>
                            </div>
                            <div class="card-footer">
                                <p class="card-text position-relative"><span id="FirstCardMonthlyCost"></span>,- /md</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md4 col-12 mb-4">
                        <div class="card h-100">
                            <div class="card-body">
                                <h5 class="card-title" id="SecondCardTitle"></h5>
                                <p class="card-text" id="SecondCardText"></p>
                                <p class="card-text" style="font-size: 10px;"><span id="SecondCardStartFee"></span><span id="SecondCardSub"></span><span id="SecondCardSub"></span><span id="SecondCardYearlyFee"></span><span id="SecondCardTerminationFee"></span></p>
                            </div>
                            <div class="card-footer">
                                <p class="card-text position-relative"><span id="SecondCardMonthlyCost"></span>,- /md</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md4 col-12 mb-4">
                        <div class="card h-100">
                            <div class="card-body">
                                <h5 class="card-title" id="ThirdCardTitle"></h5>
                                <p class="card-text" id="ThirdCardText"></p>
                                <p class="card-text" style="font-size: 10px;"><span id="ThirdCardStartFee"></span><span id="ThirdCardSub"></span><span id="ThirdCardSub"></span><span id="ThirdCardYearlyFee"></span><span id="ThirdCardTerminationFee"></span></p>
                            </div>
                            <div class="card-footer">
                                <p class="card-text position-relative"><span id="ThirdCardMonthlyCost"></span>,- /md</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="/assets/zapp/calculations.js"></script>
    <script type="text/javascript">
        // Bootstrapslider with JQuery
        var yearlykm = $("#yearlykm").slider();
        var costkWh = $('#costkWh').slider();

        yearlykm.slider({
            tooltip: 'hover'
        });

        costkWh.slider({
            tooltip: 'hover'
        });

        // Trigger the calculation
        calculateTotal();

        // Ordering the car list by alphabete
        var sel = $('#carkWhkm');
        var selected = sel.val(); // cache selected value, before reordering
        var opts_list = sel.find('option');
        opts_list.sort(function(a, b) {
            return $(a).text() > $(b).text() ? 1 : -1;
        });
        sel.html('').append(opts_list);
        sel.val(selected); // set cached selected value

    </script>
</body>

</html>
