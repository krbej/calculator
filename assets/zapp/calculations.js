function calculateTotal() {

    // Basic values
    var monthlySubscriptionZapp = 100;
    var monthlySubscriptionMonta = 75;
    var refundPrkWhZapp = 1.10;
    var terminationFeeZapp = 0;
    var yearlyFeeZapp = 0;
    var startFeeZapp = 0;
    //var boxPriceZapp = 0; - Value is entered in inputfield 'oneTimeCost'
    var cardStartFeeText = 'Opstart og installation: ';

    // First Card
    var firstCardTitle = 'AURA';
    var firstCardText = 'Ved køb af boks fra AURA.';
    var firstCardName = 'AURA';
    var firstCardMonthlySubscription = 79;
    var firstCardRefundPrkWh = 1.12;
    var firstCardTermination = 0;
    var firstCardYearlyFee = 0;
    var firstCardStartFee = 0;
    var firstCardBoxPrice = 12999;

    // Second Card
    var secondCardTitle = 'EON Home Plus';
    var secondCardText = 'Ved køb af boks fra EON.';
    var secondCardName = 'EON';
    var secondCardMonthlySubscription = 159;
    var secondCardRefundPrkWh = 1.12;
    var secondCardTermination = 0;
    var secondCardYearlyFee = 0;
    var secondCardStartFee = 0;
    var secondCardBoxPrice = 14995;

    // Third Card
    var thirdCardTitle = 'Clever All-in-One';
    var thirdCardText = 'Ved leje af boks fra Clever.';
    var thirdCardName = 'Clever';
    var thirdCardMonthlySubscription = 799;
    var thirdCardRefundPrkWh = 1.86;
    var thirdCardTermination = 2500;
    var thirdCardYearlyFee = 0;
    var thirdCardStartFee = 0;
    var thirdCardBoxPrice = 8000;




    // Trigger Zapp and Card calculations
    if (document.getElementById('Zapp').checked) {
        var monthly = calculateTotalYear(monthlySubscriptionZapp, refundPrkWhZapp, startFeeZapp, parseInt(document.addem.oneTimeCost.value), terminationFeeZapp, yearlyFeeZapp);
        document.getElementById('subCost').innerHTML = monthlySubscriptionZapp;
    } else if (document.getElementById('Monta').checked) {
        var monthly = calculateTotalYear(monthlySubscriptionMonta, refundPrkWhZapp, startFeeZapp, parseInt(document.addem.oneTimeCost.value), terminationFeeZapp, yearlyFeeZapp);
        document.getElementById('subCost').innerHTML = monthlySubscriptionMonta;
    }
    var monthlyFirstCard = calculateTotalYear(firstCardMonthlySubscription, firstCardRefundPrkWh, firstCardStartFee, firstCardBoxPrice, firstCardTermination, firstCardYearlyFee);
    var monthlySecondCard = calculateTotalYear(secondCardMonthlySubscription, secondCardRefundPrkWh, secondCardStartFee, secondCardBoxPrice, secondCardTermination, secondCardYearlyFee);
    var monthlyThirdCard = calculateTotalYear(thirdCardMonthlySubscription, thirdCardRefundPrkWh, thirdCardStartFee, thirdCardBoxPrice, thirdCardTermination, thirdCardYearlyFee);

    // Calculate and Display Zapp Price
    totalCostPower = parseInt((document.addem.carkWhkm.value / 1000) * yearlykm.slider('getValue') * costkWh.slider('getValue'));
    totalRefund = parseInt(((document.addem.carkWhkm.value / 1000) * yearlykm.slider('getValue')) * refundPrkWhZapp);
    var totalPowerCostZapp = parseFloat(totalCostPower);
    document.getElementById('powerCost').innerHTML = totalPowerCostZapp;
    var totalRefundZapp = parseFloat(totalRefund);
    document.getElementById('refund').innerHTML = totalRefundZapp;

    // Display all            
    var totalCostMonthlyIntZapp = parseFloat(monthly);
    document.getElementById('mothlyCost').innerHTML = totalCostMonthlyIntZapp;
    var firstCardMonthlyInt = parseFloat(monthlyFirstCard);
    document.getElementById('FirstCardMonthlyCost').innerHTML = firstCardMonthlyInt;
    var secondCardMonthlyInt = parseFloat(monthlySecondCard);
    document.getElementById('SecondCardMonthlyCost').innerHTML = secondCardMonthlyInt;
    var thirdCardMonthlyInt = parseFloat(monthlyThirdCard);
    document.getElementById('ThirdCardMonthlyCost').innerHTML = thirdCardMonthlyInt;

    document.getElementById('yearlykm-display').innerHTML = yearlykm.slider('getValue') + " km";
    document.getElementById('costkWh-display').innerHTML = costkWh.slider('getValue') + ",-";
    document.getElementById('refundZapp1').innerHTML = refundPrkWhZapp.toFixed(2);
    document.getElementById('refundZapp2').innerHTML = refundPrkWhZapp.toFixed(2);




    // Display values to bottom box text
    // First Card (AURA)
    document.getElementById('FirstCardTitle').innerHTML = (firstCardTitle);
    document.getElementById('FirstCardText').innerHTML = (firstCardText);
    if (firstCardStartFee + firstCardBoxPrice != 0) {
        document.getElementById('FirstCardStartFee').innerHTML = cardStartFeeText + (firstCardStartFee + firstCardBoxPrice).toLocaleString('da-DK') + " kr, ";
    }
    if (firstCardMonthlySubscription != 0) {
        document.getElementById('FirstCardSub').innerHTML = firstCardMonthlySubscription.toLocaleString('da-DK') + " kr mdr. ";
    }
    if (firstCardRefundPrkWh != 0) {
        document.getElementById('FirstCardSub').innerHTML = "Elrefusion: " + firstCardRefundPrkWh.toLocaleString('da-DK') + " kr/kWh. ";
    }
    if (firstCardYearlyFee != 0) {
        document.getElementById('FirstCardYearlyFee').innerHTML = "Årligt " + firstCardName + " kontigent: " + firstCardYearlyFee.toLocaleString('da-DK') + " kr. ";
    }
    if (firstCardTermination != 0) {
        document.getElementById('FirstCardTerminationFee').innerHTML = "Opsigelsesgebyr: " + firstCardTermination.toLocaleString('da-DK') + " kr. ";
    }

    // Second Card (EON)
    document.getElementById('SecondCardTitle').innerHTML = (secondCardTitle);
    document.getElementById('SecondCardText').innerHTML = (secondCardText);
    if (secondCardStartFee + secondCardBoxPrice != 0) {
        document.getElementById('SecondCardStartFee').innerHTML = cardStartFeeText + (secondCardStartFee + secondCardBoxPrice).toLocaleString('da-DK') + " kr, ";
    }
    if (secondCardMonthlySubscription != 0) {
        document.getElementById('SecondCardSub').innerHTML = secondCardMonthlySubscription.toLocaleString('da-DK') + " kr mdr. ";
    }
    if (secondCardRefundPrkWh != 0) {
        document.getElementById('SecondCardSub').innerHTML = "Elrefusion: " + secondCardRefundPrkWh.toLocaleString('da-DK') + " kr/kWh. ";
    }
    if (secondCardYearlyFee != 0) {
        document.getElementById('SecondCardYearlyFee').innerHTML = "Årligt " + secondCardName + " kontigent: " + secondCardYearlyFee.toLocaleString('da-DK') + " kr. ";
    }
    if (secondCardTermination != 0) {
        document.getElementById('SecondCardTerminationFee').innerHTML = "Opsigelsesgebyr: " + secondCardTermination.toLocaleString('da-DK') + " kr. ";
    }

    // Third Card (Clever)
    document.getElementById('ThirdCardTitle').innerHTML = (thirdCardTitle);
    document.getElementById('ThirdCardText').innerHTML = (thirdCardText);
    if (thirdCardStartFee + thirdCardBoxPrice != 0) {
        document.getElementById('ThirdCardStartFee').innerHTML = cardStartFeeText + (thirdCardStartFee + thirdCardBoxPrice).toLocaleString('da-DK') + " kr, ";
    }
    if (thirdCardMonthlySubscription != 0) {
        document.getElementById('ThirdCardSub').innerHTML = thirdCardMonthlySubscription.toLocaleString('da-DK') + " kr mdr. ";
    }
    if (thirdCardRefundPrkWh != 0) {
        document.getElementById('ThirdCardSub').innerHTML = "Elrefusion: " + thirdCardRefundPrkWh.toLocaleString('da-DK') + " kr/kWh. ";
    }
    if (thirdCardYearlyFee != 0) {
        document.getElementById('ThirdCardYearlyFee').innerHTML = "Årligt " + thirdCardName + " kontigent: " + thirdCardYearlyFee.toLocaleString('da-DK') + " kr. ";
    }
    if (thirdCardTermination != 0) {
        document.getElementById('ThirdCardTerminationFee').innerHTML = "Opsigelsesgebyr: " + thirdCardTermination.toLocaleString('da-DK') + " kr. ";
    }

}

function calculateTotalYear(subscription, refundAmount, startPrice, box, terminationFee, yearlyFee) {
    totalCostPower = parseInt((document.addem.carkWhkm.value / 1000) * yearlykm.slider('getValue') * costkWh.slider('getValue'));
    totalRefund = parseInt(((document.addem.carkWhkm.value / 1000) * yearlykm.slider('getValue')) * refundAmount);
    if (document.getElementById('1year').checked) {
        totalCostMonthly = subscription + (startPrice / 12) + (box / 12) + (terminationFee / 12) + ((totalCostPower - totalRefund) / 12) + (yearlyFee / 12);
    } else if (document.getElementById('2year').checked) {
        totalCostMonthly = subscription + (startPrice / 24) + (box / 24) + (terminationFee / 24) + ((totalCostPower - totalRefund) / 12) + (yearlyFee / 12);
    } else if (document.getElementById('3year').checked) {
        totalCostMonthly = subscription + (startPrice / 36) + (box / 36) + (terminationFee / 36) + ((totalCostPower - totalRefund) / 12) + (yearlyFee / 12);
    }
    return parseInt(totalCostMonthly);
}
